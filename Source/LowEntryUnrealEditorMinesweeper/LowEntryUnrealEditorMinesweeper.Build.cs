using UnrealBuildTool;
using System.IO;

public class LowEntryUnrealEditorMinesweeper : ModuleRules
{
	public LowEntryUnrealEditorMinesweeper(ReadOnlyTargetRules Target) : base(Target)
	{
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
		PCHUsage = PCHUsageMode.NoPCHs;
		bUseUnity = false;

		PublicIncludePaths.AddRange(
			new string[]
			{
				Path.Combine(ModuleDirectory, "Public/Module"),
				Path.Combine(ModuleDirectory, "Public/Classes")
			}
		);

		PrivateIncludePaths.AddRange(
			new string[]
			{
				Path.Combine(ModuleDirectory, "Private/Module"),
				Path.Combine(ModuleDirectory, "Private/Classes")
			}
		);

		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Engine",
				"Core",
				"CoreUObject",
				"BlueprintGraph"
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"EditorStyle",
				"UnrealEd",
				"GraphEditor",
				"Slate",
				"SlateCore",
				"UMG",
				"UMGEditor",
				"Blutility",
				"Projects"
			}
		);

		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
			}
		);
	}
}