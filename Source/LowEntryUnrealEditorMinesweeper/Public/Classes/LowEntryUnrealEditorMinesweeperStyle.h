// Copyright Low Entry. All Rights Reserved.

#pragma once

#include "Styling/SlateStyle.h"


class FLowEntryUnrealEditorMinesweeperStyle
{
public:
	static void Initialize();
	static void Shutdown();

private:
	static TSharedRef<FSlateStyleSet> Create();

public:
	static void SetupStyles(FSlateStyleSet* Style);

	static const ISlateStyle& Get();
	static FName GetStyleSetName();
	static void ReloadTextures();


private:
	static TSharedPtr<FSlateStyleSet> StyleInstance;
};
