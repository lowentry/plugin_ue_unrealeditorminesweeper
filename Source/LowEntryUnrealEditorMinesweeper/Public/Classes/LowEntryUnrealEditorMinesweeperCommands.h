// Copyright Low Entry. All Rights Reserved.

#pragma once

#include "LowEntryUnrealEditorMinesweeperStyle.h"

#include "Framework/Commands/Commands.h"


class FLowEntryUnrealEditorMinesweeperCommands : public TCommands<FLowEntryUnrealEditorMinesweeperCommands>
{
public:
	FLowEntryUnrealEditorMinesweeperCommands() : TCommands<FLowEntryUnrealEditorMinesweeperCommands>(TEXT("LowEntryUnrealEditorMinesweeper"), NSLOCTEXT("Contexts", "LowEntryUnrealEditorMinesweeper", "Low Entry Unreal Editor Minesweeper Plugin"), NAME_None, FLowEntryUnrealEditorMinesweeperStyle::GetStyleSetName()) {}

	virtual void RegisterCommands() override;


public:
	TSharedPtr<FUICommandInfo> OpenMinesweeperTab;
};
