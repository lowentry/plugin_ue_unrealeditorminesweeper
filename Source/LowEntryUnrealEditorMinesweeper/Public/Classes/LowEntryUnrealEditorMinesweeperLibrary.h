#pragma once


#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "LowEntryUnrealEditorMinesweeperLibrary.generated.h"


class UWorld;


UCLASS()
class ULowEntryUnrealEditorMinesweeperLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()
public:
	/**
	* Returns the static editor world.
	*/
	UFUNCTION(BlueprintPure, Category = "Low Entry|Unreal Editor Minesweeper", Meta = (DisplayName = "Get Editor World"))
	static UWorld* GetEditorWorld();


	/**
	* Returns the static data model.
	*/
	UFUNCTION(BlueprintCallable, Category = "Low Entry|Unreal Editor Minesweeper", Meta = (DisplayName = "Get Minesweeper Data Model"))
	static UObject* GetMinesweeperDataModel();

	/**
	* Sets the static data model.
	*/
	UFUNCTION(BlueprintCallable, Category = "Low Entry|Unreal Editor Minesweeper", Meta = (DisplayName = "Set Minesweeper Data Model"))
	static void SetMinesweeperDataModel(UObject* Model);


private:
	static UObject* DataModel;
};
