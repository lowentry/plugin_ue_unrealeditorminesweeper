// Copyright Low Entry. All Rights Reserved.

#include "LowEntryUnrealEditorMinesweeperCommands.h"

#define LOCTEXT_NAMESPACE "FLowEntryUnrealEditorMinesweeperCommands"


void FLowEntryUnrealEditorMinesweeperCommands::RegisterCommands()
{
	UI_COMMAND(OpenMinesweeperTab, "Minesweeper", "Shows the minesweeper window", EUserInterfaceActionType::Button, FInputChord());
}


#undef LOCTEXT_NAMESPACE
