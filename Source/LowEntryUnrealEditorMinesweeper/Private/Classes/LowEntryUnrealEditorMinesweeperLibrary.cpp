// Copyright Low Entry. All Rights Reserved.

#include "LowEntryUnrealEditorMinesweeperLibrary.h"

#include "Editor.h"
#include "Engine/World.h"


UObject* ULowEntryUnrealEditorMinesweeperLibrary::DataModel = nullptr;


ULowEntryUnrealEditorMinesweeperLibrary::ULowEntryUnrealEditorMinesweeperLibrary(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}


UWorld* ULowEntryUnrealEditorMinesweeperLibrary::GetEditorWorld()
{
	if (!GEditor)
	{
		return nullptr;
	}
	return GEditor->GetEditorWorldContext().World();
}


UObject* ULowEntryUnrealEditorMinesweeperLibrary::GetMinesweeperDataModel()
{
	return DataModel;
}

void ULowEntryUnrealEditorMinesweeperLibrary::SetMinesweeperDataModel(UObject* Model)
{
	if (DataModel)
	{
		DataModel->RemoveFromRoot();
	}
	if (Model)
	{
		Model->AddToRoot();
	}
	DataModel = Model;
}
