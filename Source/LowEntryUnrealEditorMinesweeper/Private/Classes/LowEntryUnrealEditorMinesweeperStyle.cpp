// Copyright Low Entry. All Rights Reserved.

#include "LowEntryUnrealEditorMinesweeperStyle.h"

#include "Interfaces/IPluginManager.h"
#include "Styling/SlateStyleRegistry.h"
#include "Framework/Application/SlateApplication.h"


TSharedPtr<FSlateStyleSet> FLowEntryUnrealEditorMinesweeperStyle::StyleInstance = nullptr;


void FLowEntryUnrealEditorMinesweeperStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FLowEntryUnrealEditorMinesweeperStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}


TSharedRef<FSlateStyleSet> FLowEntryUnrealEditorMinesweeperStyle::Create()
{
	TSharedRef<FSlateStyleSet> Style = MakeShareable(new FSlateStyleSet(GetStyleSetName()));
	Style->SetContentRoot(IPluginManager::Get().FindPlugin(TEXT("LowEntryUEMinesweeper"))->GetBaseDir() / TEXT("Resources"));
	SetupStyles(&Style.Get());
	return Style;
}

void FLowEntryUnrealEditorMinesweeperStyle::SetupStyles(FSlateStyleSet* Style)
{
	Style->Set("LowEntryUnrealEditorMinesweeper.OpenMinesweeperTab", new FSlateImageBrush(Style->RootToContentDir(TEXT("Images/toolbar.png")), FVector2D(40, 40)));
	Style->Set("LowEntryUnrealEditorMinesweeper.OpenMinesweeperTab.Small", new FSlateImageBrush(Style->RootToContentDir(TEXT("Images/toolbar.png")), FVector2D(20, 20)));
	Style->Set("LowEntryUnrealEditorMinesweeper.OpenMinesweeperTab.Selected", new FSlateImageBrush(Style->RootToContentDir(TEXT("Images/toolbar.png")), FVector2D(40, 40)));

	Style->Set("LowEntryUnrealEditorMinesweeper.MinesweeperTab", new FSlateImageBrush(Style->RootToContentDir(TEXT("Images/tabicon.png")), FVector2D(40, 40)));
	Style->Set("LowEntryUnrealEditorMinesweeper.MinesweeperTab.Small", new FSlateImageBrush(Style->RootToContentDir(TEXT("Images/tabicon.png")), FVector2D(20, 20)));
}


const ISlateStyle& FLowEntryUnrealEditorMinesweeperStyle::Get()
{
	return *StyleInstance;
}

FName FLowEntryUnrealEditorMinesweeperStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("LowEntryUnrealEditorMinesweeperStyle"));
	return StyleSetName;
}

void FLowEntryUnrealEditorMinesweeperStyle::ReloadTextures()
{
	if (FSlateApplication::IsInitialized())
	{
		FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
	}
}
