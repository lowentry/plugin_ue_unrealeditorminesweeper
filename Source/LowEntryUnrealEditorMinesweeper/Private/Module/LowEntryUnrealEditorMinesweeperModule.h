// Copyright Low Entry. All Rights Reserved.

#pragma once


#include "ILowEntryUnrealEditorMinesweeperModule.h"


class FSpawnTabArgs;
class FToolBarBuilder;
class FUICommandList;
class SDockTab;


class FLowEntryUnrealEditorMinesweeperModule : public ILowEntryUnrealEditorMinesweeperModule
{
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	void OpenMinesweeperTab();

private:
	void AddToolbarExtension(FToolBarBuilder& Builder);
	TSharedRef<SDockTab> OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs);


private:
	TSharedPtr<FUICommandList> Commands;
};
