// Copyright Low Entry. All Rights Reserved.

#include "LowEntryUnrealEditorMinesweeperModule.h"
#include "LowEntryUnrealEditorMinesweeperStyle.h"
#include "LowEntryUnrealEditorMinesweeperCommands.h"

#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "EditorUtilityWidgetBlueprint.h"
#include "EditorUtilityWidget.h"
#include "Widgets/Docking/SDockTab.h"
#include "LevelEditor.h"
#include "Engine/World.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"


#define LOCTEXT_NAMESPACE "FLowEntryUnrealEditorMinesweeperModule"


static const FName MinesweeperTabName("LowEntryMinesweeper");


void FLowEntryUnrealEditorMinesweeperModule::StartupModule()
{
	FLowEntryUnrealEditorMinesweeperStyle::Initialize();
	FLowEntryUnrealEditorMinesweeperStyle::ReloadTextures();
	FLowEntryUnrealEditorMinesweeperCommands::Register();

	Commands = MakeShareable(new FUICommandList());
	Commands->MapAction(
		FLowEntryUnrealEditorMinesweeperCommands::Get().OpenMinesweeperTab,
		FExecuteAction::CreateRaw(this, &FLowEntryUnrealEditorMinesweeperModule::OpenMinesweeperTab),
		FCanExecuteAction()
	);

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
	TSharedPtr<FExtender> ToolbarExtender = MakeShareable(new FExtender());
	ToolbarExtender->AddToolBarExtension("Settings", EExtensionHook::After, Commands, FToolBarExtensionDelegate::CreateRaw(this, &FLowEntryUnrealEditorMinesweeperModule::AddToolbarExtension));
	LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(ToolbarExtender);

	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(MinesweeperTabName, FOnSpawnTab::CreateRaw(this, &FLowEntryUnrealEditorMinesweeperModule::OnSpawnPluginTab))
		.SetDisplayName(LOCTEXT("MinesweeperTabTitle", "Minesweeper"))
		.SetMenuType(ETabSpawnerMenuType::Enabled)
		.SetIcon(FSlateIcon(FLowEntryUnrealEditorMinesweeperStyle::GetStyleSetName(), "LowEntryUnrealEditorMinesweeper.MinesweeperTab.Small"));
}

void FLowEntryUnrealEditorMinesweeperModule::ShutdownModule()
{
	FLowEntryUnrealEditorMinesweeperStyle::Shutdown();
	FLowEntryUnrealEditorMinesweeperCommands::Unregister();

	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(MinesweeperTabName);
}


void FLowEntryUnrealEditorMinesweeperModule::OpenMinesweeperTab()
{
	FLevelEditorModule& LevelEditorModule = FModuleManager::GetModuleChecked<FLevelEditorModule>(TEXT("LevelEditor"));
	LevelEditorModule.GetLevelEditorTabManager()->TryInvokeTab(MinesweeperTabName);
}


void FLowEntryUnrealEditorMinesweeperModule::AddToolbarExtension(FToolBarBuilder& Builder)
{
	Builder.AddToolBarButton(FLowEntryUnrealEditorMinesweeperCommands::Get().OpenMinesweeperTab);
}

TSharedRef<SDockTab> FLowEntryUnrealEditorMinesweeperModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	TSharedRef<SDockTab> CreatedTab = SNew(SDockTab).TabRole(NomadTab);
	UEditorUtilityWidgetBlueprint* MinesweeperTabBlueprint = Cast<UEditorUtilityWidgetBlueprint>(FSoftObjectPath("/LowEntryUEMinesweeper/Blueprints/Widgets/Minesweeper.Minesweeper").TryLoad());
	if (MinesweeperTabBlueprint && GEditor)
	{
		UWorld* World = GEditor->GetEditorWorldContext().World();
		if (World)
		{
			UEditorUtilityWidget* MinesweeperTab = CreateWidget<UEditorUtilityWidget>(World, MinesweeperTabBlueprint->GeneratedClass.Get());
			CreatedTab->SetContent(MinesweeperTab->TakeWidget());
		}
	}
	return CreatedTab;
}


#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FLowEntryUnrealEditorMinesweeperModule, LowEntryUnrealEditorMinesweeper)
